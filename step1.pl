% **************************************************************************** #
%                                                           LE - /             #
%                                                               /              #
%    step1.pl                                         .::    .:/ .      .::    #
%                                                  +:+:+   +:    +:  +:+:+     #
%    By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+      #
%                                                  #+#   #+    #+    #+#       #
%    Created: 2018/01/14 18:59:14 by jmonneri     #+#   ##    ##    #+#        #
%    Updated: 2018/01/14 18:59:14 by jmonneri    ###    #+. /#+    ###.fr      #
%                                                          /                   #
%                                                         /                    #
% **************************************************************************** #

femme(F) :- member(F, [anne, betty, lisa, sylvie, eve, julie, valerie]).
homme(H) :- member(H, [marc, luc, jean, jules, leon, loic, gerard, jacques, herve, paul]).

mari_de(H, F) :- member((H, F), [(marc, anne), (luc, betty), (jules, lisa), (leon, sylvie), (loic, eve), (paul, julie)]).
femme_de(F, H) :- mari_de(H, F).

beaupere_de(B, E) :- (mari_de(E, M); femme_de(E, M)), pere_de(B, M).
bellemere_de(B, E) :- beaupere_de(M, E), femme_de(B, M).

pere_de(H, E) :- member((E, H), [(jean, marc), (jules, marc), (leon, marc), (lisa, luc), (loic, luc), (gerard, luc), (jacques, jules), (herve, leon), (julie, leon), (paul, loic), (valerie, loic)]).
mere_de(F, E) :- pere_de(H, E), femme_de(F, H).
enfant_de(E, P) :- pere_de(P, E); mere_de(P, E).

ancetre_de(A, D) :- enfant_de(A, D); (enfant_de(P, A), enfant_de(D, P)).
