% **************************************************************************** #
%                                                           LE - /             #
%                                                               /              #
%    step2.pl                                         .::    .:/ .      .::    #
%                                                  +:+:+   +:    +:  +:+:+     #
%    By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+      #
%                                                  #+#   #+    #+    #+#       #
%    Created: 2018/01/14 19:06:50 by jmonneri     #+#   ##    ##    #+#        #
%    Updated: 2018/01/14 19:06:50 by jmonneri    ###    #+. /#+    ###.fr      #
%                                                          /                   #
%                                                         /                    #
% **************************************************************************** #

appartient_a(X, [X|_]).
appartient_a(X, [_|Y]) :- appartient_a(X, Y).

a_gauche_de(A, B, [A, B|_]).
a_gauche_de(A, B, [_|C]) :- a_gauche_de(A, B, C).

voisin_de(A, B, [A, B|_]).
voisin_de(A, B, [B, A|_]).
voisin_de(A, B, [_|C]) :- voisin_de(A, B, C).

resolve(Enigme) :-
	Enigme = [m(_, _, _, _, _), m(_, _, _, _, _), m(_, _, _, _, _), m(_, _, _, _, _), m(_, _, _, _, _)],
	appartient_a(m(anglais, rouge, _, _, _), Enigme),
	appartient_a(m(suedois, _, chiens, _, _), Enigme),
	appartient_a(m(danois, _, _, the, _), Enigme),
	a_gauche_de(m(_, verte, _, _, _), m(_, blanche, _, _, _), Enigme),
	appartient_a(m(_, verte, _, cafe, _), Enigme),
	appartient_a(m(_, _, oiseaux, _, pall-mall), Enigme),
	appartient_a(m(_, jaune, _, _, dunhill), Enigme),
	Enigme = [m(_, _, _, _, _), m(_, _, _, _, _), m(_, _, _, lait, _), m(_, _, _, _, _), m(_, _, _, _, _)],
	Enigme = [m(norvegien, _, _, _, _), m(_, _, _, _, _), m(_, _, _, _, _), m(_, _, _, _, _), m(_, _, _, _, _)],
	voisin_de(m(_, _, _, _, blend), m(_, _, chats, _, _), Enigme),
	voisin_de(m(_, _, cheval, _, _), m(_, _, _, _, dunhill), Enigme),
	appartient_a(m(_, _, _, biere, blue-master), Enigme),
	appartient_a(m(allemand, _, _, _, prince), Enigme),
	voisin_de(m(norvegien, _, _, _, _), m(_, bleue, _, _, _), Enigme),
	voisin_de(m(_, _, _, _, blend), m(_, _, _, eau, _), Enigme),
	appartient_a(m(_, _, poisson, _, _), Enigme).
